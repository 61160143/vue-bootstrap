const User = require('../model/User')
const userController = {
  userList: [
    { id: 1, name: 'Felidas', gender: 'M' },
    { id: 2, name: 'Storie', gender: 'M' }
  ],
  lastId: 3,
  async addUser (req, res, next) {
    const payload = req.body
    //res.json(companyController.addCompany(payload))
    console.log(payload)
    const user = new User(payload)
    try {
      user.save()
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateUser (req, res, next)  {
    const payload = req.body
    try {
      console.log(payload)
      const user = await User.updateOne({ _id: payload._id }, payload)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteUser (req, res, next) {
    const { id } = req.params
    try{
      const user = await User.deleteOne({ _id: id})
    res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getUsers (req, res, next) {
    User.find({}).then(function (user) {
      res.json(user)
    }).catch(function (err) {
      res.status(500).send(err)
    })
  },
  async getUser (req, res, next)  {
    const { id } = req.params
    User.findById(id).then(function (user) {
      res.json(user)
    }).catch(function (err) {
      res.status(500).status(err)
    })
  }
}
module.exports = userController
